'use strict';

var chai = require('chai');
var should = chai.should();
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

var userid = "pwc";
var passphrase = "This is my test 123# passphrase that talks like a ninja monkey called keynja";
var pgp = require('../')(userid,passphrase);
var aes= require('../')(userid,passphrase, "aes");
var data = "some data to encrypt";

describe('PGP encryption test',function(){
	this.timeout(45000);
	describe('Test PGP key pairs generation',function(){
		it('should generate pgp privatekey',function(done){	
			pgp.generateKeyPair().should.eventually.have.property('privateKey').notify(done);
		});

		it('should generate pgp publickey',function(done){
			pgp.generateKeyPair().should.eventually.have.property('publicKey').notify(done);
		});
	});

	describe('Test data encryption and decryption',function(){
		it('should encrypt and decrypt data',function(done){
			var publick;
			var privatek;
			var encryptedData;
			pgp.generateKeyPair()
			.then(function(res){
				publick = res.publicKey;
				privatek = res.privateKey;
				return pgp.encryptData(publick,data);
			})
			.then(function(res){
				encryptedData = res;
				console.log(encryptedData);
				return pgp.decryptData(privatek,encryptedData);
			})
			.then(function(res){
				console.log(res);
				done();
			})
			.catch(function(err){
				console.log(err);
			});
		});
		});
	// it('should encryptdata',function(done){
	// 	pgp.encryptData(publick,data).should.be.fullfilled.notify(done);
	// });

	// it('should decryptdata',function(done){
	// 	pgp.decryptData(privatek,encryptedData).should.be.fullfilled.notify(done);
	// });
});

describe('AES encryption test',function(){
	this.timeout(45000);
	describe('Test AES key pairs generation',function(){
		it('should generate AES privatekey',function(done){	
			aes.generateKeyPair().should.eventually.have.property('privateKey').notify(done);
		});

		it('should generate AES publickey',function(done){
			aes.generateKeyPair().should.eventually.have.property('publicKey').notify(done);
		});
	});

	describe('Test data encryption and decryption',function(){
		it('should encrypt and decrypt data',function(done){
			var publick;
			var privatek;
			var encryptedData;
			aes.generateKeyPair()
			.then(function(res){
				publick = res.publicKey;
				privatek = res.privateKey;
				return aes.encryptData(publick,data);
			})
			.then(function(res){
				encryptedData = res;
				console.log(encryptedData);
				return aes.decryptData(privatek,encryptedData);
			})
			.then(function(res){
				console.log(res);
				done();
			})
			.catch(function(err){
				console.log(err);
			});
		});
		});
	// it('should encryptdata',function(done){
	// 	pgp.encryptData(publick,data).should.be.fullfilled.notify(done);
	// });

	// it('should decryptdata',function(done){
	// 	pgp.decryptData(privatek,encryptedData).should.be.fullfilled.notify(done);
	// });
});