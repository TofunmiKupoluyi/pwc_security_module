'use strict';

var openpgp = require('openpgp');
var q = require('q');
var gibberish = require("gibberish-aes/dist/gibberish-aes-1.0.0.js");

module.exports = function(userId,passphrase,type){
	var options = {
		numBits: 2048,
		userId: userId,
		passphrase: passphrase
	};
	return {
		generateKeyPair: function(){
			var deferred = q.defer();
			if(type=="aes"){
				function randomString(){
					var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
					var string_length = 18;
					var randomstring = '';
					for (var i=0; i<string_length; i++) {
						var rnum = Math.floor(Math.random() * chars.length);
						randomstring += chars.substring(rnum,rnum+1);
					}
					return randomstring;
					}
					var key= randomString();
					deferred.resolve({
						privateKey: key,
						publicKey: key
				});
			}

			else{
				openpgp.generateKeyPair(options)
				.then(function(keypair) {
					var privkey = keypair.privateKeyArmored;
					var pubkey = keypair.publicKeyArmored;
					deferred.resolve(
					{
						privateKey:privkey,
						publicKey:pubkey
					}
					);
				}).catch(function(error) {
					deferred.reject(error);
				});
			}
			return deferred.promise;
		},

		encryptData: function(publicKeyString, data){
			var deferred = q.defer();
			if(type=="aes"){
				var key= publicKeyString;
				var encryptedText= gibberish.enc(data, publicKeyString);
				deferred.resolve(encryptedText);
			}
			else{
				var key = publicKeyString;
				var publicKey = openpgp.key.readArmored(key);
				openpgp.encryptMessage(publicKey.keys, data)
				.then(function(pgpMessage) {
					deferred.resolve(pgpMessage);
				}).catch(function(error) {
					deferred.reject(error);
				});
			}
			return deferred.promise;
		},

		decryptData: function(privateKeyString,pgpMessageString){
			var deferred = q.defer();
			if(type=="aes"){
				var key= privateKeyString;
				var decryptedText=gibberish.dec(pgpMessageString, key);
				deferred.resolve(decryptedText);
			}
			else{
				var key = privateKeyString;
				var privateKey = openpgp.key.readArmored(key).keys[0];
				privateKey.decrypt(passphrase);

				var pgpMessage = pgpMessageString;
				pgpMessage = openpgp.message.readArmored(pgpMessage);

				openpgp.decryptMessage(privateKey, pgpMessage)
				.then(function(plaintext) {
					deferred.resolve(plaintext);
				}).catch(function(error) {
					deferred.reject(error);
				});
			}
			return deferred.promise;
		}
	}

}